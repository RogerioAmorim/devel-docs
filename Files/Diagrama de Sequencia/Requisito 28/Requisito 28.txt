@startuml

autonumber
actor Jogador
boundary Cliente

Jogador -> Cliente: Jogador envia uma mensagem em uma partida ativa.
Cliente -> Cliente: Prepara o comando: gmsg < nickname > < mensagem > para enviar para o servidor
Cliente -> Servidor: Envia o comando em hexadecimal.
Servidor -> Cliente : Envia mensagem para o clientes usando a resposta: gmsg < nickname > < mensagem >  em formato Hexadecimal.
Cliente -> Cliente : Traduz a mensagem de Hexadecimal para String.
Cliente -> Cliente : Mostra a mensagem no batepapo da partida
Cliente -> Jogador : Notifica os jogadores da nova mensagem

@enduml
