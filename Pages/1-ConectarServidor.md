﻿


#  Requisito 1 - Conectar no Servidor
## 1 - Resumo
**História de Usuário**
>Como usuário, gostaria de entrar em um partida disponível em um servidor

**Pré-condições**<br />
>O usuário fornece um nick e um servidor para conectar

**Pós-condições**
>1. **Fluxo Normal** <br />
    1.1. O usuário conecta em uma partida e ganha um número de 1 a 6 que o identifica;<br/>

>2. **Fluxo Alternativo** <br/>
	2.1 - O sistema informa que a partida está lotada (já tem 6 jogadores)  <br/>
	2.2 - O sistema informa que não conseguiu conexão com o servidor;  <br/>
	2.3 - O sistema informa que o nick é protegido por senha; <br/>

**Observações** 
>O protocolo tetrinet não é padronizado, tem versões diferentes de protocolo, falta documentação e há implementações diferentes de servidores.  
Isso pode tornar o comportamento diferente conforme as decisões de implementação, por exemplo:  
>- o servidor tem suporte a várias salas de jogos?  
>- o servidor suporta nicknames protegidos por senha?  
Como o desenvolvimento é incremental, nada impede de se começar com servidores mais simples e implementar as funcionalidades gradualmente. Link do protocolo: https://gitlab.com/tetrinetjs/protocol/-/jobs/200969009/artifacts/file/tetrinetProtocol.pdf

## 2 - Desenvolvedor
**Protocolo**
>TCP

**Mensagens**
###### Observação: Todas as mensagens são enviadas no formato Hexadecimal.


>1. **Enviadas para o Servidor** <br/>
> --- **Codificação**: Essa mensagem **é codificada**!<br/>
> --- **Mensagem enviada**: **tetrisstart < nickname>< version >**<br/>
> --- **Parâmetros**:<br/>
> --------- nickname:string => Nick que o usuário forneceu;<br/>
> --------- version:string => O número da versão da aplicação cliente;<br/>
   
> **Exemplo de envio**:<br/>
 > --------- em **Hexadecimal**: 4571aed84040acebee7ae1a5725c53f82a56fe65ae035e035ff303034323946323341303343393733433835443037424438434430333446383343323033353446353645443433323531423345353246364141454437ff<br/>
> --------- **Codificado**: 00429F23A03C973C85D07BD8CD034F83C20354F56ED43251B3E52F6AAED7ÿ<br/>
> --------- **Decodificado**: tetrisstart gtetrinet545 1.13<br/>

>2. **Recebidas do Servidor** <br/>
>**Observação**: O servidor envia essa mensagem para todos os clientes ativos, através de **comunicação sockets**<br/>
> --- **Codificação**: Essa mensagem **não é codificada**!<br/>
> --- **Mensagem enviada**: **playernum < iDJogador > playerjoin < iDJogador > < nickname > winlist  List{< type >< nickname >;< points >} team < iDJogador > < team > **<br/>
> --- **Parâmetros**:<br/>
> --------- iDJogador:number => Id do jogador;<br/>
> --------- team:string => Nome do time que o jogador vai participar;<br/>
 > --------- type:char => Qual o tipo do vencedor (t = Team ou p = Player)<br/>
> --------- nickname:string => Nickname do Jogador/Time<br/>
> --------- points:number => Pontuação do Jogador/Time <br/>
>**Exemplo de envio**:<br/>
> --------- em **Hexadecimal**: 706c617965726e756d203120706c617965726a6f696e2031206774657472696e657435342077696e6c6973742074546f757254686541627973733b39323220704672656767653b3835352070656c697373613b343735207465616d20312054696d655665726d656c686ff<br/>
> --------- em **Texto imprimivel**: playernum 1 playerjoin 1 gtetrinet54 winlist tTourTheAbyss;922 pFregge;855 pelissa;475 team 1 TimeVermelho<br/>
## 3 - Diagrama de sequência

![Requisito 1](https://gitlab.com/tetrinetjs/devel-docs/raw/jonathas/Files/Diagrama%20de%20Sequencia/Requisito%201/Requisito%201%20-%20ConectarServidor.png)



